# Note: this is a dummy file to show how to use the pre-commit hook

#!/bin/sh

echo "Running pre-commit hook..."

# Define the tag that indicates skipping linting
SKIP_TAG="lolo"

# Get the last commit message
COMMIT_MESSAGE=$(git log -1 --pretty=%B)

echo "Commit message: $COMMIT_MESSAGE" # Debugging line

# Check for the skip tag in the commit message
if echo "$COMMIT_MESSAGE" | grep -q "$SKIP_TAG"; then
    echo "Skipping linting due to tag in commit message."
    exit 0  # Exit with zero to skip the linting
else
    echo "No skip tag found, proceeding with linting." # Debugging line
fi

new_sql_files=$(git diff --name-only $target_commit..$current_commit | grep '\.sql$')
echo "New SQL files in this MR:"
echo "$new_sql_files"


# Perform SQLFluff linting and fixing here
# Adjust these commands as per your project setup
for file in $new_sql_files; do
    echo "Checking file: $file"
    if [ -f "$file" ]; then
        if [[ $file =~ sf_.*\.sql ]]; then
            echo "Linting $file with Snowflake rules"
            sqlfluff fix --config .sqlfluff_snowflake $file -f
        elif [[ $file =~ mysql_.*\.sql ]]; then
            echo "Linting $file with MySQL rules"
            sqlfluff fix --config .sqlfluff_mysql $file -f
        else
            echo "No specific linter configuration for $file"
        fi
    else
        echo "File $file does not exist. Skipping..."
    fi
done


# Check for SQLFluff errors or any other commands you want to run
