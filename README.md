# SQL Linting Configuration

This repository contains a setup for linting SQL files using [SQLFluff](https://github.com/sqlfluff/sqlfluff) with separate configurations for MySQL and Snowflake dialects.

## Linting MySQL and Snowflake SQL Files

To lint SQL files in this repository, we have set up separate linting configurations for MySQL and Snowflake dialects. The linting process is based on the file prefix:

- Files starting with `mysql_` will be linted using the MySQL-specific linting configuration.
- Files starting with `sf_` will be linted using the Snowflake-specific linting configuration.

## Configuration Files

- `.sqlfluff_mysql`: MySQL-specific linting configuration.
- `.sqlfluff_snowflake`: Snowflake-specific linting configuration.

Make sure to customize these configuration files with your specific linting rules, settings, and options for each dialect.

## GitLab CI/CD Pipeline

We use GitLab CI/CD to automate the linting process during Merge Requests (MRs). The pipeline is configured to identify SQL files in the MR and apply the appropriate linting configuration based on the file prefix.

Here's how it works:
1. When a new MR is created, the pipeline checks for new SQL files.
2. SQLFluff is run on each SQL file, using the corresponding dialect-specific configuration.

## Usage

To use this setup for linting SQL files in your project, follow these steps:

1. Customize the `.sqlfluff_mysql` and `.sqlfluff_snowflake` configuration files with your desired linting rules.

2. Include this README in your project repository to document the setup.

3. Make sure that GitLab CI/CD is configured to run the linting job on MRs.

4. Prefix your SQL files with `mysql_` for MySQL-specific linting or `sf_` for Snowflake-specific linting.

5. Create an MR, and the linting process will automatically apply the correct configuration.

## Contributing

Feel free to contribute to this setup by improving linting configurations, updating documentation, or suggesting enhancements. We welcome your contributions!
