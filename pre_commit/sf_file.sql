create table staging_db.price_per_variant
as
select
  id as variant_id
  , sku as variant_sku
  , suggested_price
  , cost_price
  , price
  , created_at
  , updated_at as updated_at
  , date_trunc('day', created_at) as created_at_day
from
  fivetran_database.hw_admin_us_hw_admin.spree_variants;
