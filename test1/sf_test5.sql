/* Get suggested price per variant */

create table staging_db.price_per_variant as
select
  id as variant_id
  , sku
  , suggested_price
from
  fivetran_database.hw_admin_us_hw_admin.spree_variants;
